package com.auction.model;

import com.auction.listener.IThreadListener;

import java.util.ArrayList;

import java.util.TimerTask;

/**
 * Created by ShubU on 4/28/2017 AD.
 */
public class Item extends TimerTask{
    private IThreadListener timeoutListener;
    private String ownerName;
    private String itemName;
    private String itemDesc;
    private double startBid;
    private boolean isEnded = false;
    private ArrayList<BidHistory> histories;
    private int auctionTime; //auctionTime will be inputed in second format

    
    public Item(String ownerName, String itemName, String itemDesc, double startBid, int auctionTime) {
        this.ownerName = ownerName;
        this.itemName = itemName;
        this.itemDesc = itemDesc;
        this.startBid = startBid;
        this.auctionTime = auctionTime;
        histories = new ArrayList<BidHistory>();
    }

    @Override
    public synchronized void run() { //synchronized just for safety

        if (this.auctionTime == 0)
            this.notifyTimeOut();
        else
            this.auctionTime -= 1; //reduce time every second
    }

    public void placeBid(String name, double amount){
        this.histories.add(new BidHistory(amount, name, 0));
    }

    public void setTimeoutListener(IThreadListener listener) {
        this.timeoutListener = listener;
    }

    public void notifyTimeOut(){
        this.timeoutListener.updateTimeout(this);
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getItemName() {
        return itemName;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public boolean getIsEnded(){
        return this.isEnded;
    }

    public double getStartBid() {
        return startBid;
    }

    public ArrayList<BidHistory> getHistories() {
        return histories;
    }

    public int getAuctionTime() {
        return auctionTime;
    }


}
