package com.auction.model;

/**
 * Created by ShubU on 4/28/2017 AD.
 */
public class BidHistory {

    private double amount;
    private String bidderName;
    private int timeLeft;

    public BidHistory(double amount, String bidderName, int timeLeft) {
        this.amount = amount;
        this.bidderName = bidderName;
        this.timeLeft = timeLeft;
    }

    public double getAmount() {
        return amount;
    }

    public String getBidderName() {
        return bidderName;
    }

    public int getTimeLeft() {
        return timeLeft;
    }
}
