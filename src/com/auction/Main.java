package com.auction;

import com.auction.auctionBack.AuctionServer;
import com.auction.auctionBack.IAuctionServer;
import com.auction.auctionMonitor.Monitor;
import com.auction.factory.AuctionServerFactory;

public class Main {

    public static void main(String[] args) {
        IAuctionServer server = AuctionServerFactory.createAuctionServer();
        Monitor m = new Monitor(server);
        
    }
}
