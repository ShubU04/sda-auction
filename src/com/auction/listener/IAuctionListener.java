package com.auction.listener;

import com.auction.model.Item;

/**
 * Created by ShubU on 4/28/2017 AD.
 */
public interface IAuctionListener {
    void update(Item item);
}
