package com.auction.auctionBack;


import com.auction.listener.IAuctionListener;
import com.auction.model.Item;

import java.util.ArrayList;

public interface IAuctionServer {

    public void placeItemForBid(String ownerName, String itemName,
                                String itemDesc, double startBid, int auctionTime);
    public void bidOnItem(String bidderName, String itemName,
                          double bid);
    public ArrayList<Item> getItems();
    public Item getItem(String itemName);
    public void registerListener(IAuctionListener al, String itemName);

    public void notifyListener(Item item);

}


