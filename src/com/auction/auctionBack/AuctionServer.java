package com.auction.auctionBack;


import com.auction.listener.IAuctionListener;
import com.auction.listener.IThreadListener;
import com.auction.model.BidHistory;
import com.auction.model.Item;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

/**
 * Created by ShubU on 4/28/2017 AD.
 */

public class AuctionServer implements IAuctionServer, IThreadListener {
    private Map<Item, Timer> bidItemList;
    private Map<IAuctionListener, Item> listeners;


    public AuctionServer(){
        listeners = new HashMap<IAuctionListener,Item>();
        bidItemList = new HashMap<Item,Timer>();
        System.out.println("sever started...");

    }

    public BidHistory getWinnerOf(String itemName){
        Item item = this.getItem(itemName);
        if (!item.getIsEnded())
            throw new IllegalArgumentException("the bid hasn't ended yet!!! ");
        return item.getHistories().get(item.getHistories().size() - 1);
    }

    @Override
    public void placeItemForBid(String ownerName, String itemName, String itemDesc, double startBid, int auctionTime) {
        if (this.getItem(itemName) != null)
            throw new IllegalArgumentException("item already existed!! change item name!!!");
        Item item = new Item(ownerName,itemName,itemDesc,startBid,auctionTime);
        bidItemList.put(item, new Timer(true));
        bidItemList.get(item).scheduleAtFixedRate(item, 0, 1000);

    }

    @Override
    public void bidOnItem(String bidderName, String itemName, double bid) {
        System.out.println("bid on item : " + itemName + " by " + bidderName);
        Item item = this.getItem(itemName);
        if (item == null)
            throw new IllegalArgumentException("Item not found!!!");
        item.placeBid(bidderName, bid);
    }

    @Override
    public ArrayList<Item> getItems() {
        ArrayList<Item> itemList = new ArrayList<Item>();
        this.bidItemList.forEach( (item, timer) -> {
            itemList.add(item);
        });
        return itemList;
    }

    @Override
    public Item getItem(String itemName) {
        return bidItemList.keySet().stream()
                .filter( item -> item.getItemName().equals(itemName))
                .findAny()
                .orElse(null);
    }

    @Override
    public void registerListener(IAuctionListener al, String itemName) {
        Item founded = this.getItem(itemName);
        if (founded == null)
            throw new IllegalArgumentException("item out found");
        this.listeners.put(al,founded);
    }

    //notify any listener who listen to this item
    @Override
    public void notifyListener(Item item) {
        this.listeners.forEach( (listener, i) -> {
            if (i.getOwnerName().equals(item.getOwnerName()))
                listener.update(item);
        });
    }

    @Override
    public void updateTimeout(Item itemName) {
        this.bidItemList.get(itemName).cancel();
        //last bid history in item will be the winner!!!
        System.out.println("the winner of this item is : " + itemName.getHistories().get(itemName.getHistories().size() - 1));
    }


}
