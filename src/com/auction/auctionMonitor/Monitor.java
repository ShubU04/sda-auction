package com.auction.auctionMonitor;

import com.auction.auctionBack.AuctionServer;
import com.auction.auctionBack.IAuctionServer;
import com.auction.listener.IAuctionListener;
import com.auction.model.Item;
import com.auction.recorder.AuctionRecorder;

import java.util.ArrayList;

/**
 * Created by ShubU on 4/28/2017 AD.
 */


public class Monitor implements IAuctionListener {

    AuctionRecorder recoder;
    IAuctionServer auctionServer;
    ArrayList<Item> items;

    public Monitor(IAuctionServer server){
        recoder = new AuctionRecorder();
        auctionServer = server;
        items = auctionServer.getItems();
    }
    
    public void registerItem(String itemName){
        this.auctionServer.registerListener(this, itemName);
    }


    @Override
    public void update(Item item) {
        System.out.println("item changed : " + item.getItemName());
        Item tempItem = items.stream().filter( i  -> item.getOwnerName().equals(i.getOwnerName()))
                .findAny()
                .orElse(null);
        tempItem = item; //reassign item into current itemList
    }


}
