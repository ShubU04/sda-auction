package com.auction.factory;

import com.auction.auctionBack.AuctionServer;
import com.auction.auctionBack.IAuctionServer;

/**
 * Created by ShubU on 4/29/2017 AD.
 */
public class AuctionServerFactory {
    static private IAuctionServer auctionServer;
    public static IAuctionServer createAuctionServer(){
        if (auctionServer == null){
            return auctionServer = new AuctionServer();
        }
        else
            return auctionServer;
    }
}
