package com.auction.recorder;

import com.auction.model.Item;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by ShubU on 4/29/2017 AD.
 */


public class AuctionRecorder {
    private String filename;

    public AuctionRecorder(){

    }

    public void setFilename(String filename){
        this.filename = filename;
    }

    public void record(Item item){
        try{
            PrintWriter writer = new PrintWriter(this.filename, "UTF-8");
            writer.println("The first line");
            writer.println("The second line");
            writer.close();
        } catch (IOException e) {
            // do something
        }
    }


}
